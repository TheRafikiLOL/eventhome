# EventHome

## Índice

1. [Introducción](#introducción)
2. [Requisitos](#requisitos-📋)
3. [Montaje del entorno local](#montaje-del-entorno-local-⚙️)

## Introducción

El objetivo principal de este proyecto es aprender nuevas tecnologías que se puedan emplear en el desarrollo de aplicaciones web.

EventHome es una aplicación web desarrollada en Angular. Su función principal es mostrar a los usuarios que visiten la página toda la información de los eventos almacenados en la base de datos, registrados por los organizadores que participan subiendo la información de cada uno de los eventos.

Los usuarios podrán visitar las distintas secciones de la web para ver la información de los eventos que han sido registrados y, si están registrados, interactuar con el contenido para una mejor experiencia.

## Requisitos 📋

Para hacer funcionar este proyecto en un entorno local tengo instalado el siguiente sofware:
- MongoDB versión 4.0.24.
- NodeJS versión 14.15.1.
- Angular versión 11.2.6.

## Montaje del entorno local ⚙️

Este proyecto se despliega en un entorno local, para hacer se usa el puerto 27017, que es el puerto predeterminado de MongoDB, el puerto 3900 para la API de NodeJS y el puerto 4200 para la aplicación Angular.
Para hacer funcionar el proyecto hay que seguir los siguientes pasos:

1. Descargaremos el proyecto alojado en el [repositorio de GitLab](https://gitlab.com/TheRafikiLOL/eventhome). Descargaremos la versión que se encuentra en la rama *“master”*. [https://gitlab.com/TheRafikiLOL/eventhome](https://gitlab.com/TheRafikiLOL/eventhome)
Una vez descargado el proyecto procederemos a obtener los directorios *“node_modules”* tanto del directorio *“backend”* como en *“EventHome”*.
2. Desde el terminal accederemos al directorio *“backend”*, alojado en el directorio raíz, donde ejecutaremos el comando `npm install`. Este comando comenzará a descargar los módulos necesarios para hacer funcionar la API introduciéndolos en el directorio *“node_modules”*.
3. Haremos el mismo procedimiento desde el directorio *“EventHome”*, alojado en el directorio raíz. Ejecutaremos el comando `npm install` y se nos descargará el directorio *“node modules”* junto a los módulos necesarios de la aplicación Angular.
4. Con todos los archivos necesarios para la ejecución completa del proyecto procederemos a ejecutar la terminal de MongoDB, donde se alojarán los datos. Esta aplicación establecerá su funcionamiento en el puerto 27017 del entorno local.
5. Iniciaremos el programa de la API, para ello accederemos al directorio *“backend”* donde, en un terminal, ejecutaremos el comando *“node index.js”*. Esto realizará una conexión con la base de datos de nombre *“eventHome”* que nos provee MongoDB.
6. Finalmente, accederemos al directorio *“EventHome”*, donde encontraremos la aplicación Angular. Allí ejecutaremos el comando *“ng serve”*, que compilará la aplicación y nos dará acceso a ella en el entorno local del puerto 4200.
7. Para acceder a la aplicación usaremos un navegador web y iremos a la ruta local [localhost:4200](http://localhost:4200/).
