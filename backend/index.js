'use strict'

var dbURL = 'mongodb://localhost:27017/eventHome';

var mongoose = require('mongoose');
var app = require('./app');
var port = 3900;

mongoose.set('useFindAndModify', false);

mongoose.Promise = global.Promise;

mongoose.connect(dbURL, { useNewUrlParser: true })
.then(() => {
    console.log("Conectado a la base de datos correctamente");

    app.listen(port, () => {
        console.log('Servidor corriendo en http://localhost:'+port);
    });
});