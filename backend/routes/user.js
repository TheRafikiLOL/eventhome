'use strict'

var express = require('express');
var UserController = require('../controllers/user');
var jwt = require('jsonwebtoken');

var router = express.Router();

// Rutas usuarios
router.post('/register', UserController.register);
router.post('/login', UserController.login);
router.get('/userId', verifyToken, UserController.getUserId);
router.get('/profile/:id', UserController.getUser);
router.post('/profile/:id', UserController.updateUser);
router.delete('/profile/:id', UserController.delete);

// Rutas páginas
router.get('/users', UserController.getUsers);
router.get('/organizers', UserController.getOrganizers);

module.exports = router;

function verifyToken(req, res, next){
    if (!req.headers.authorization) {
        return res.status(401).send({
            status: 'error',
            mesage: 'No puedes acceder a esta ruta.'
        });
    }

    const token = req.headers.authorization.split(' ');

    //console.log(token)

    if (token === 'null'){
        return res.status(401).send({
            status: 'error',
            mesage: 'No puedes acceder a esta ruta.'
        });
    }

    const payload = jwt.verify(token[1], 'secretKey')
    //console.log(payload)

    req.userId = payload._id;
    next();
}
