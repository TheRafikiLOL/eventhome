'use strict'

var express = require('express');
var ValorationController = require('../controllers/valoration');

var router = express.Router();

router.post('/valoration', ValorationController.valorate);
router.get('/valorations/:id', ValorationController.allEventValorations);

module.exports = router;