'use strict'

var express = require('express');
var EventController = require('../controllers/event');

var router = express.Router();

var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: './upload/events' });

// Rutas eventos
router.post('/publish', EventController.publish);
router.get('/events/:page?', EventController.getEvents);
router.get('/lastEvents/:last', EventController.getLastEvents);
router.get('/organizerEvents/:id', EventController.getOrganizerEvents);
router.get('/event/:id', EventController.getEvent);
router.put('/event/:id', EventController.update);
router.delete('/event/:id', EventController.delete);
router.post('/upload-image/:id?', md_upload, EventController.upload);
router.get('/get-image/:image', EventController.getImage);
router.delete('/get-image/:image', EventController.deleteImage);
router.get('/search/:search', EventController.search);

module.exports = router;