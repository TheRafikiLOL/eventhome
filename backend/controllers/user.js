'use strict'

var validator = require('validator');
var fs = require('fs');
var path = require('path');
var jwt = require('jsonwebtoken');
var bcrypt=require('bcrypt-nodejs');

var User = require('../models/user');
var Event = require('../models/event');
var Valoration = require('../models/valoration');

var controller = {

    register: (req, res) => {
        const { name, email, password, passwordSecure, rol } = req.body;

        // Comprovamos si el correo está registrado
        User.findOne({email}, function(err, findUser) {
            if (findUser) {
                return res.status(401).send({
                    status: 'error',
                    content: 'El correo electrónico ya pertenece a un usuario.'
                });
            }

            // Validamos la contraseña
            if (password != passwordSecure) {
                return res.status(401).send({
                    status: 'error',
                    content: 'Las contraseñas no coinciden.'
                });
            }

            // Validamos el rol del usuario
            if (rol != "organizer" && rol != "visitor") {
                return res.status(401).send({
                    status: 'error',
                    content: 'El rol del usuario no es válido.'
                });
            }

            // Registramos el usuario
            let user = new User({name, email, password, rol});

            // Encriptamos la contraseña
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(user.password, salt, null, (err, hash) => {
                    if (err) {
                        next(err);
                    }
                    user.password = hash;
                });

                user.save();

                const token = jwt.sign({ _id: user._id }, 'secretKey');
                
                return res.status(200).send({
                    status: 'success',
                    token
                });
            });            
        });
    },

    login: (req, res) => {
        const { email, password } = req.body;

        // Buscamos el correo en la base de datos
        User.findOne({email}, function(err, user) {
            if (!user) {
                return res.status(401).send({
                    status: 'error',
                    content: 'No existe un usuario con ese correo'
                });
            }

            // Comparamos con la contraseña pasada
            bcrypt.compare(password, user.password, function(err, result) {
                if (!result) {
                    return res.status(401).send({
                        status: 'error',
                        content: 'Contraseña incorrecta'
                    });
                }

                // Generamos y devolvemos el token
                const token = jwt.sign({ _id: user._id}, 'secretKey');

                return res.status(200).send({
                    status: 'success',
                    token
                });
            });
        });
    },

    getUserId: (req, res) => {
        let userId = req.userId

        return res.status(200).send({
            status: 'success',
            userId
        });
    },

    getUser: (req, res) => {
        
        // Recoger el id de la url
        var userId = req.params.id;

        // Comprobar que existe
        if(!userId || userId == null){
            return res.status(404).send({
                status: 'error',
                message: 'El usuario no existe'
            });
        }

        // Buscar objeto
        User.findById(userId, (err, user) => {
            if(err || !user){
                return res.status(404).send({
                    status: 'error',
                    message: 'El usuario no existe'
                });
            }

            //Devolverlo en json
            return res.status(200).send({
                status: 'success',
                user
            });

        });

    },

    getUsers: (req, res) => {

        var query = User.find({ rol: "user" });

        // Find
        query.sort('-_id').exec((err, users) => {

            if(err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los usuarios'
                });
            }

            if(!users) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay usuarios registrados'
                });
            }

            return res.status(200).send({
                status: 'success',
                users
            });
        });
    },

    getOrganizers: (req, res) => {

        var query = User.find({ rol: "organizer" });

        // Find
        query.sort('-_id').exec((err, organizers) => {

            if(err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los usuarios'
                });
            }

            if(!organizers) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay usuarios registrados'
                });
            }

            return res.status(200).send({
                status: 'success',
                organizers
            });
        });
    },

    updateUser: (req, res) => {
        
        // Recoger el id
        var userId = req.params.id;

        // Recoger los datos
        var params = req.body;

        // Validar los datos
        try{
            var validate_name = !validator.isEmpty(params.name);
        }catch(err){
            return res.status(500).send({
                status: 'error',
                mesage: 'Faltan datos por enviar' 
            });
        }

        if(validate_name){
            // Find and update
            User.findByIdAndUpdate({_id: userId}, params, {new: true}, (err, userUpdated) => {
                if(err){
                    return res.status(500).send({
                        status: 'error',
                        message: 'Error al actualizar'
                    });
                }

                if(!userUpdated){
                    return res.status(404).send({
                        status: 'error',
                        message: 'No existe el usuario para actualizar'
                    });
                }

                return res.status(200).send({
                    status: 'success',
                    userUpdated
                });
            });
        }else{
            // Devolver consulta
            return res.status(200).send({
                status: 'error',
                message: 'No existe el usuario'
            });
        }
    },

    delete: (req, res) => {
        // Recoger el id
        var userId = req.params.id;

        // Buscamos la información del usuario
        User.findById(userId, (err, user) => {
            if(err){
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al buscar el usuario'
                });
            }

            if(!user){
                return res.status(404).send({
                    status: 'error',
                    message: 'No se ha encontrado el usuario a eliminar'
                });
            }

            // Identificamos si es organización o visitante
            if (user.rol == 'visitor') {

                // Buscamos las valoracions por la ID del usuario
                Valoration.find({ userId: user._id }).exec((err, valorations) => {

                    if(err) {
                        return res.status(500).send({
                            status: 'error',
                            message: 'Error al devolver las valoraciones'
                        });
                    }

                    // Si no hay valoraciones borramos el usuario
                    if(!valorations || valorations.length == 0) {

                        User.findOneAndDelete({_id: user._id}, (err, userRemoved) => {
                            if(err){
                                return res.status(500).send({
                                    status: 'error',
                                    message: 'Error al borrar'
                                });
                            }
                
                            return res.status(200).send({
                                status:'success' ,
                                user: userRemoved
                            });
                        });
                    }

                    // Borramos las valoraciones encontradas
                    for (let i = 0; i < valorations.length; i++) {
                        
                        Valoration.findOneAndDelete({_id: valorations[i]._id}, (err, valorationRemoved) => {
                    
                            if(err){
                                return res.status(404).send({
                                    status: 'error',
                                    message: 'Ha habido un error al borrar la valoración'
                                });
                            }
        
                            if (i == valorations.length-1) {
                                
                                // Eliminamos el usuario
                                User.findOneAndDelete({_id: user._id}, (err, userRemoved) => {
                                    if(err){
                                        return res.status(500).send({
                                            status: 'error',
                                            message: 'Error al borrar'
                                        });
                                    }
                        
                                    return res.status(200).send({
                                        status:'success' ,
                                        user: userRemoved
                                    });
                                });

                            }
                        
                        })

                    }

                });

            }
            else if (user.rol == 'organizer') {

                // Buscamos los eventos por la ID del usuario
                Event.find({ organizerId: user._id }).exec((err, events) => {

                    if(err){
                        return res.status(500).send({
                            status: 'error',
                            message: 'Error al borrar'
                        });
                    }

                    // Si no hay eventos borramos el usuario
                    if(!events || events.length == 0) {

                        User.findOneAndDelete({_id: user._id}, (err, userRemoved) => {
                            if(err){
                                return res.status(500).send({
                                    status: 'error',
                                    message: 'Error al borrar'
                                });
                            }
                
                            return res.status(200).send({
                                status:'success' ,
                                user: userRemoved
                            });
                        });
                    }

                    // Recorremos los eventos
                    for (let i = 0; i < events.length; i++) {

                        Valoration.find({ eventId: events[i]._id }).exec((err, valorations) => {

                            if(err) {
                                return res.status(500).send({
                                    status: 'error',
                                    message: 'Error al encontrar las valoraciones'
                                });
                            }

                            // Si el evento no tiene valoraciones lo eliminamos
                            if(!valorations || valorations.length == 0) {

                                Event.findOneAndDelete({_id: events[i]._id}, (err, eventRemoved) => {
                                    if(err){
                                        return res.status(500).send({
                                            status: 'error',
                                            message: 'Error al borrar'
                                        });
                                    }
                                });
                            }
                            // Si el evento tiene valoraciones las eliminamos
                            else {
                                for (let x = 0; x < valorations.length; x++) {

                                    Valoration.findOneAndDelete({_id: valorations[x]._id}, (err, valorationRemoved) => {
                                        
                                        if(err || !valorationRemoved){
                                            return res.status(404).send({
                                                status: 'error',
                                                message: 'Ha habido un error al borrar la valoración'
                                            });
                                        }
                    
                                        // Al final se elimina el evento
                                        if (x == valorations.length-1) {
                                            Event.findOneAndDelete({_id: events[i]._id}, (err, eventRemoved) => {
                                                if(err){
                                                    return res.status(500).send({
                                                        status: 'error',
                                                        message: 'Error al borrar'
                                                    });
                                                }
    
                                            });
                                        }
                                    
                                    })
                                }                          
                            }

                        });

                        if (i == events.length-1) {
                                
                                // Eliminamos el usuario
                                User.findOneAndDelete({_id: user._id}, (err, userRemoved) => {
                                    if(err){
                                        return res.status(500).send({
                                            status: 'error',
                                            message: 'Error al borrar'
                                        });
                                    }
                        
                                    return res.status(200).send({
                                        status:'success' ,
                                        user: userRemoved
                                    });
                                });

                        }

                    }

                })                
            }
            else {
                return res.status(500).send({
                    status: 'error',
                    message: 'El tipo de usuario no es correcto'
                });
            }

        });

    }

};

module.exports = controller;
