'use strict'

var validator = require('validator');
var fs = require('fs');
var path = require('path');

var Event = require('../models/event');
var User = require('../models/user');
var Valoration = require('../models/valoration');

var controller = {

    publish: (req, res) => {
        // Recoger parametros por post
        var params = req.body;

        // Validar datos (validator)
        try {
            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.content);

        }catch(err){
            return res.status(200).send({
                status: 'error',
                message: 'Faltan datos por enviar'
            });
        }

        if(validate_title && validate_content){
            // Crear el objeto a guardar
            var event = new Event();

            // Asignar valores
            event.organizerId = params.organizerId;
            event.title = params.title;
            event.date = params.date;
            event.content = params.content;

            if(params.image){
                event.image = params.image;
            }
            else {
                event.image = null;
            }

            // Guardar el objeto
            event.save((err, eventStored) => {
                
                if(err || !eventStored){
                    return res.status(404).send({
                        status: 'error',
                        message: 'El evento no se ha podido guardar'
                    });
                }

                // Devolver una respuesta
                return res.status(200).send({
                    status: 'success',
                    event: eventStored
                });

            });
        }
        else {
            return res.status(200).send({
                status: 'error',
                message: 'Los datos no son válidos'
            });
        }
    },

    getEvents: (req, res) => {

        let page = "";

        if (req.params.page || req.params.page != undefined) {
            page = parseInt(req.params.page);
        }
        else {
            page = 1;
        }

        let limit = 6;

        const startIndex = (page - 1) * limit;
        const endIndex = page * limit

        // Find
        Event.find().sort('-_id').exec((err, events) => {

            if(err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los eventos'
                });
            }

            if(!events) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay eventos registrados'
                });
            }

            const paginate = {};

            if (startIndex > 0) {
                paginate.previous = { page: (page - 1), limit }
            }

            if (endIndex < events.length) {
                paginate.next = { page: (page + 1), limit }
            }

            return res.status(200).send({
                status: 'success',
                paginate,
                events : events.slice(startIndex, endIndex)
            });
        });
    },

    getLastEvents: (req, res) => {

        var query = Event.find();

        var last = req.params.last;
        if(last || last != undefined){
            query.limit(6);
        }

        // Find
        query.sort('-_id').exec((err, events) => {

            if(err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los eventos'
                });
            }

            if(!events) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay eventos registrados'
                });
            }

            return res.status(200).send({
                status: 'success',
                events
            });
        });
    },

    getOrganizerEvents: (req, res) => {

        // Buscamos los eventos con id del organizador
        Event.find({ organizerId: req.params.id }).sort([['date', 'descending']]).exec((err, events) => {

            if(err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver los eventos'
                });
            }

            if(!events) {
                return res.status(404).send({
                    status: 'error',
                    message: 'No se han encontrado eventos'
                });
            }

            return res.status(200).send({
                status: 'success',
                events
            });

        });
        
    },

    getEvent: (req, res) => {
        
        // Recoger el id de la url
        var eventId = req.params.id;

        // Comprobar que existe
        if(!eventId || eventId == null){
            return res.status(404).send({
                status: 'error',
                message: 'El evento no existe'
            });
        }

        // Buscar objeto
        Event.findById(eventId, (err, event) => {
            if(err || !event){
                return res.status(404).send({
                    status: 'error',
                    message: 'El evento no existe'
                });
            }

            // Obtenemos la información de la organización

            let organizerName = "";
            
            // Buscar objeto
            User.findById(event.organizerId, (err, user) => {

                organizerName = user.name;

                return res.status(200).send({
                    status: 'success',
                    event,
                    organizerName
                });
            });

        });

    },

    update: (req, res) => {
        // Recoger el id
        var eventId = req.params.id;

        // Recoger los datos
        var params = req.body;

        // Validar los datos
        try{
            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.content);
        }catch(err){
            return res.status(500).send({
                status: 'error',
                mesage: 'Faltan datos por enviar' 
            });
        }

        if(validate_title && validate_content){
            // Find and update
            Event.findByIdAndUpdate({_id: eventId}, params, {new: true}, (err, eventUpdated) => {
                if(err){
                    return res.status(500).send({
                        status: 'error',
                        message: 'Error al actualizar'
                    });
                }

                if(!eventUpdated){
                    return res.status(404).send({
                        status: 'error',
                        message: 'No existe el evento para actualizar'
                    });
                }

                return res.status(200).send({
                    status: 'success',
                    eventUpdated
                });
            });
        }else{
            // Devolver consulta
            return res.status(200).send({
                status: 'error',
                message: 'No existe el evento'
            });
        }
    },

    delete: (req, res) => {
        // Recoger el id
        var eventId = req.params.id;

        // Buscamos las valoraciones del evento
        Valoration.find({ eventId }).exec((err, valorations) => {

            if(err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al encontrar las valoraciones'
                });
            }

            // Elimina el evento sin valoraciones
            if(!valorations || valorations.length == 0) {
                Event.findOneAndDelete({_id: eventId}, (err, eventRemoved) => {
                    if(err){
                        return res.status(500).send({
                            status: 'error',
                            message: 'Error al borrar'
                        });
                    }
        
                    if(!eventRemoved){
                        return res.status(404).send({
                            status: 'error',
                            message: 'No se ha encontrado el evento a eliminar'
                        });
                    }
        
                    return res.status(200).send({
                        status:'success' ,
                        event: eventRemoved
                    });
                });
            }

            // Eliminamos las valoraciones encontradas

            for (let i = 0; i < valorations.length; i++) {

                Valoration.findOneAndDelete({_id: valorations[i]._id}, (err, valorationRemoved) => {
                    
                    if(err || !valorationRemoved){
                        return res.status(404).send({
                            status: 'error',
                            message: 'Ha habido un error al borrar la valoración'
                        });
                    }

                    if (i == valorations.length-1) {
                        // Elimina el evento sin valoraciones
                        Event.findOneAndDelete({_id: eventId}, (err, eventRemoved) => {
                            if(err){
                                return res.status(500).send({
                                    status: 'error',
                                    message: 'Error al borrar'
                                });
                            }
                
                            if(!eventRemoved){
                                return res.status(404).send({
                                    status: 'error',
                                    message: 'No se ha encontrado el evento a eliminar'
                                });
                            }
                
                            return res.status(200).send({
                                status:'success',
                                event: eventRemoved
                            });
                        });
                    }
                
                })
            }

        });
    },

    upload: (req, res) => {
        // Recoger el fichero de la petición
        var file_name = 'Imagen no subida...';

        if(!req.files){
            return res.status(404).send({
                status: 'error',
                message: file_name
            });
        }

        // Conseguir nombre y extensión del archivo
        var file_path = req.files.file0.path;

        // Nombre del archivo
        var file_name = file_path.split('\\')[2];
        // * ADVERTENCIA * EN LINUX O MAC
        //var file_name = file_path.split('/')[2];

        // Extension del fichero
        var file_ext = file_name.split('.')[1];

        // Comprobar la extensión, solo imagenes, si no es valida borrar el fichero
        if(file_ext != 'png' && file_ext != 'jpg' && file_ext != 'jpeg' && file_ext != 'PNG' && file_ext != 'JPG' && file_ext != 'JPEG'){
            // borrar el archivo subido
            fs.unlink(file_path, (err) => {
                return res.status(200).send({
                    status: 'error',
                    message: 'La extensión de la imagen no es válida'
                });
            });
        }
        else {

            // Si todo es valido, sacando id de la url
            var eventId = req.params.id;

            if(eventId){
                // Actualizar el objeto con el nombre de la imagen
                Event.findOneAndUpdate({_id: eventId}, {image: file_name}, {new:true}, (err, eventUpdated) => {

                    if(err || !eventUpdated){
                        return res.status(200).send({
                            status: 'error',
                            message: 'Error al guardar la imagen de articulo'
                        });
                    }

                    return res.status(200).send({
                        status: 'success',
                        article: eventUpdated
                    });
                });
            }
            else {
                return res.status(200).send({
                    status: 'success',
                    image: {image: file_name},
                });
            }
        }
    },

    getImage: (req, res) => {
        var file = req.params.image;
        var path_file = './upload/events/'+file;

        fs.exists(path_file, (exists) => {

            if(exists){
                return res.sendFile(path.resolve(path_file));
            }
            else {
                return res.status(404).send({
                    status: 'error',
                    message: 'La imagen no existe'
                });
            }
        });
    },

    deleteImage: (req, res) => {
        var file = req.params.image;
        var path_file = './upload/events/'+file;

        fs.exists(path_file, (exists) => {

            if(exists){
                fs.unlink(path_file, (err) => {
                    return res.status(200).send({
                        status: 'success',
                        message: 'Imágen borrada con exito'
                    });
                });
            }
            else {
                return res.status(404).send({
                    status: 'error',
                    message: 'La imagen no éxiste'
                });
            }
        });
    },

    search: (req, res) => {
        // Sacar el string a buscar
        var searchString = req.params.search;

        // Find or
        Event.find({ "$or": [
            { "title": { "$regex": searchString, "$options": "i"} },
            { "content": { "$regex": searchString, "$options": "i"} }
        ]})
        .sort([['date', 'descending']])
        .exec((err, events) => {

            if(err){
                return res.status(500).send({
                    status: 'error',
                    message: 'Error en la petición'
                });
            }

            if(!events || events.length <= 0){
                return res.status(404).send({
                    status: 'error',
                    message: 'No se han encontrado eventos'
                });
            }

            return res.status(200).send({
                status: 'success',
                events
            });

        });

    }

};

module.exports = controller;
