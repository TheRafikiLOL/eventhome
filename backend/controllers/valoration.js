var validator = require('validator');

var Valoration = require('../models/valoration')
//var Event = require('../models/event');
var User = require('../models/user');

var controller = {

    valorate: (req, res) => {

        // Recoger parametros por post
        var params = req.body;

        // Validar datos (validator)
        try {
            var validate_userId = !validator.isEmpty(params.userId);
            var validate_eventId = !validator.isEmpty(params.eventId);
            var validate_rate = !validator.isEmpty(params.rate);
            var validate_content = !validator.isEmpty(params.content);

        }catch(err){
            return res.status(200).send({
                status: 'error',
                message: 'Faltan datos por enviar'
            });
        }

        if(validate_userId && validate_eventId && validate_rate && validate_content){
            // Crear el objeto a guardar
            var valoration = new Valoration();

            // Asignar valores
            valoration.userId = params.userId;
            valoration.eventId = params.eventId;
            valoration.rate = params.rate;
            valoration.content = params.content;

            // Guardar el objeto
            valoration.save((err, valorationStored) => {
                
                if(err || !valorationStored){
                    return res.status(404).send({
                        status: 'error',
                        message: 'La valoración no se ha podido guardar'
                    });
                }

                // Devolver una respuesta
                return res.status(200).send({
                    status: 'success',
                    valoration: valorationStored
                });

            });
        }
        else {
            return res.status(200).send({
                status: 'error',
                message: 'Los datos no son válidos'
            });
        }

    },

    allEventValorations: (req, res) => {

        // Buscamos las valoracions por la ID del evento
        Valoration.find({ eventId: req.params.id }).exec((err, valorations) => {

            if(err) {
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al devolver las valoraciones'
                });
            }

            if(!valorations) {
                return res.status(404).send({
                    status: 'error',
                    message: 'Sin valoraciones'
                });
            }

            // Asignamos el nombre del usuario de la valoración

            for (let i = 0; i < valorations.length; i++) {

                User.findById(valorations[i].userId, (err, user) => {
                    
                    if(err || !user){
                        return res.status(404).send({
                            status: 'error',
                            message: 'El usuario no existe'
                        });
                    }
        
                    // Lo asignamos al json
                    valorations[i].userName = user.name;

                    if (i == valorations.length-1) {
                        return res.status(200).send({
                            status: 'success',
                            valorations
                        });
                    }
                
                })
            }

        });
        
    },
    
}

module.exports = controller;