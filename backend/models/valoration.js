'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ValorationSchema = Schema({
    userId: String,
    userName: String,
    eventId: String,
    rate: Number,
    content: String
});

module.exports = mongoose.model('Valoration', ValorationSchema);