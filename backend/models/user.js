'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
    name: {type: String, required: true},
    email: {type: String, unique: true, lowercase: true, required: true},
    password: {type: String, required: true},
    bio: {type: String},
    rol: {type: String, required: true}
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);