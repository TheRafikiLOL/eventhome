'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EventSchema = Schema({
    organizerId: String,
    title: String,
    content: String,
    date: {type:Date, default:Date.now},
    image: String
});

module.exports = mongoose.model('Event', EventSchema);