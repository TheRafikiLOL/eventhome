import  { ModuleWithProviders } from '@angular/core';
import  { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';

// Componentes principales
import { HomeComponent } from './components/home/home.component';
import { EventsComponent } from './components/events/events.component';
import { EventsSearchComponent } from './components/events-search/events-search.component';
import { OrganizationsComponent } from './components/organizations/organizations.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

// Componentes de usuarios
import { ProfileComponent } from './components/profile/profile.component';
import { ProfileEditComponent } from './components/profile-edit/profile-edit.component';

// Componentes de eventos
import { EventComponent } from './components/event/event.component';
import { EventNewComponent } from './components/event-new/event-new.component';
import { EventEditComponent } from './components/event-edit/event-edit.component';

const appRoutes: Routes = [
    // Rutas principales
    //{path: '', component: HomeComponent},
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: 'home', component: HomeComponent},
    {path: 'events', component: EventsComponent},
    {path: 'events/:page', component: EventsComponent},
    {path: 'events/search/:search', component: EventsSearchComponent},
    {path: 'organizations', component: OrganizationsComponent},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},

    // Rutas de usuarios
    {path: 'profile/:id', component: ProfileComponent},
    {path: 'profile/edit/:id', component: ProfileEditComponent},

    // Rutas de eventos
    {path: 'event/:id', component: EventComponent},
    {path: 'new-event', component: EventNewComponent},
    {path: 'event/edit/:id', component: EventEditComponent},

    // Ruta 404
    {path: '**', component: NotFoundComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);