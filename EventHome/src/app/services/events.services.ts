import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Event } from '../models/event';
import { Global } from './global';

@Injectable()
export class EventService{

    public events:Event[];
    public url: string;

    constructor(
        private _http: HttpClient
    ){
        this.url = Global.url;
    }

    getEvent(id):Observable<any>{
        return this._http.get(this.url + 'event/'+id);
    }

    getEvents(page):Observable<any>{
        return this._http.get(this.url + 'events/'+page);
    }

    getLastEvents(){
        return this._http.get(this.url + 'lastEvents/4');
    }

    getOrganizerEvents(id){
        return this._http.get(this.url + 'organizerEvents/'+id);
    }

    publishEvent(event):Observable<any> {
        let params = JSON.stringify(event);
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.post(this.url+'publish', params, {headers});
    }

    updateEvent(id, event):Observable<any> {
        let params = JSON.stringify(event);
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.put(this.url + 'event/'+id, params, {headers});
    }

    deleteEvent(id):Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        
        return this._http.delete(this.url + 'event/'+id);
    }

    search(searchString):Observable<any>{
        return this._http.get(this.url + 'search/'+searchString);
    }

    getEventValorations(id):Observable<any>{
        return this._http.get(this.url + 'valorations/'+id);
    }

    valorate(valoration):Observable<any> {
        let params = JSON.stringify(valoration);
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.post(this.url+'valoration', params, {headers});
    }
}