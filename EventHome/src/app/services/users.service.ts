import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { Global } from './global';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public events:User[];
  public url: string;

  constructor(
    private _http: HttpClient
  ) {
    this.url = Global.url;
  }

  getOrganizers():Observable<any>{
    return this._http.get(this.url + 'organizers');
  }

  getUserId(token) {
    return this._http.get<any>(this.url + 'userId', token);
  }

  getUser(userId):Observable<any>{
    return this._http.get(this.url + 'profile/'+userId);
  }

  getUsers():Observable<any>{
    return this._http.get(this.url + 'users');
  }

  updateUser(id, user):Observable<any>{
    let params = JSON.stringify(user);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this._http.post(this.url + 'profile/'+id, params, {headers});
  }

  deleteUser(id):Observable<any> {
    //let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.delete(this.url + 'profile/'+id);
}
}