import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Global } from './global';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public url: string;

  constructor(private _http: HttpClient, private router: Router) {
    this.url = Global.url;
  }
  
  register(user) {
    return this._http.post<any>(this.url + 'register', user);
  }

  registerOrganizer(userId) {
    return this._http.post<any>(this.url + 'register-organizer', userId);
  }

  login(user) {
    return this._http.post<any>(this.url + 'login', user);
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
    $('.modal-backdrop.fade.show').remove()
    //location.reload();
  }

  isLogged() {
    return !!localStorage.getItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }
  
}