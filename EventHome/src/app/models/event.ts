export class Event{
    constructor(
        public _id: string,
        public organizerId: string,
        public title: string,
        public content: string,
        public date: any,
        public image: string){}
}