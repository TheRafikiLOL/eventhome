export class User{
    constructor(
        public _id: string,
        public name: string,
        public email: string,
        public bio: string,
        public rol: string,
        public image: string){}
}