export class Valoration{
    constructor(
        public _id: string,
        public userId: string,
        public userName: string,
        public eventId: string,
        public rate: number,
        public content: string){}
}