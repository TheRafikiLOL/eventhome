import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { routing, appRoutingProviders } from './app.routing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AngularFileUploaderModule } from 'angular-file-uploader';
import { CKEditorModule } from 'ng2-ckeditor';

import { AppComponent } from './app.component';
import { EventsComponent } from './components/events/events.component';
import { HeaderComponent } from './components/partials/header/header.component';
import { SliderComponent } from './components/partials/slider/slider.component';
import { FooterComponent } from './components/partials/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { OrganizationsComponent } from './components/organizations/organizations.component';
import { EventComponent } from './components/event/event.component';
import { EventNewComponent } from './components/event-new/event-new.component';
import { EventEditComponent } from './components/event-edit/event-edit.component';
import { EventsSearchComponent } from './components/events-search/events-search.component';
import { EventsLastComponent } from './components/partials/events-last/events-last.component';

import { AuthGuard } from './auth.guard';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { ProfileEditComponent } from './components/profile-edit/profile-edit.component';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { SearchComponent } from './components/partials/search/search.component';


@NgModule({
  declarations: [
    AppComponent,
    EventsComponent,
    HeaderComponent,
    SliderComponent,
    FooterComponent,
    HomeComponent,
    NotFoundComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    OrganizationsComponent,
    EventComponent,
    EventNewComponent,
    EventEditComponent,
    EventsSearchComponent,
    EventsLastComponent,
    ProfileEditComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpClientModule,
    AngularFileUploaderModule,
    CKEditorModule,
    IvyCarouselModule
  ],
  providers: [
    appRoutingProviders,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
