import { Component, OnInit } from '@angular/core';
import { Event } from '../../models/event';
import { EventService } from '../../services/events.services';
import { Global } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
  providers: [EventService]
})
export class EventsComponent implements OnInit {

  public titulo: string;
  public events: Array<Event>;
  public paginate: string;
  public url: string;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _eventService: EventService
  ) {
    this.url = Global.url
  }

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      let page = params['page'];

      if (!page || page == undefined) {
        page = 1;
      }

      this._eventService.getEvents(page).subscribe(
        response => {
          if (response.events) {
            this.events = response.events;
            this.paginate = response.paginate;
          }
          else {
  
          }
        },
        error => {
          console.error(error);
        }
      )
    });
  }

  dateFormat(date) {
    const datepipe: DatePipe = new DatePipe('en-US')
    return datepipe.transform(date, 'dd/MM/YYYY')
  }

}