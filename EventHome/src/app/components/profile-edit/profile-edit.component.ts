import { Component, OnInit, ViewChild } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { User } from '../../models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { Global } from '../../services/global';
import { CKEditorComponent } from 'ng2-ckeditor';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css'],
  providers: [UsersService]
})
export class ProfileEditComponent implements OnInit {

  public editUser: User;
  public status: string;
  public url: string;
  public loguedUserId: string;

  public editorConfig: any = {
    toolbarGroups: [
      { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
      { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
      { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
      { name: 'forms', groups: [ 'forms' ] },
      { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
      { name: 'links', groups: [ 'links' ] },
      { name: 'insert', groups: [ 'insert' ] },
      '/',
      { name: 'styles', groups: [ 'styles' ] },
      { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
      { name: 'colors', groups: [ 'colors' ] },
      { name: 'tools', groups: [ 'tools' ] },
      { name: 'others', groups: [ 'others' ] },
      { name: 'about', groups: [ 'about' ] }
    ],
    removeButtons: 'Source,Save,NewPage,Templates,ExportPdf,Preview,Print,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,NumberedList,BulletedList,Indent,Outdent,Blockquote,CreateDiv,JustifyCenter,JustifyLeft,BidiLtr,BidiRtl,Language,JustifyRight,JustifyBlock,Image,Flash,HorizontalRule,PageBreak'
  }

  constructor(
    private _userService: UsersService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.editUser = new User('','','','','',null);
    this.url = Global.url;
  }

  ngOnInit(): void {
    this._userService.getUserId(localStorage.token).subscribe(
      res => {
        this.loguedUserId = res['userId'];
        this.getUser();
      },
      err => {
        console.log(err);
        this._router.navigate(['/home']);
      }
    );
  }

  updateUser() {
    this._userService.updateUser(this.editUser._id, this.editUser).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.status == 'success';
          this.editUser = res['userUpdated'];
          this._router.navigate(['/profile/', this.editUser._id]);
        }
      },
      err => {
        console.error(err);
        this.status = "error";
      }
    );
  }

  imageUpload(data){
    this.editUser.image = data.body.image.image;
  }

  getUser(){
    this._route['params'].subscribe(params => {
      let id = params['id'];

      this._userService.getUser(id).subscribe(
        response => {
          if (response.user) {
            if (response.user._id != this.loguedUserId) {
              this._router.navigate(['/home']);
            }
            this.editUser = response.user;
          }
          else {
            this._router.navigate(['/home']);
          }
        },
        error => {
          console.error(error);
          this._router.navigate(['/home']);
        }
      )
    });
  }

}
