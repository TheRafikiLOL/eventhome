import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Event } from '../../models/event';
import { Valoration } from 'src/app/models/valoration';
import { EventService } from 'src/app/services/events.services';
import { UsersService } from 'src/app/services/users.service';
import { AuthService } from 'src/app/services/auth.service';
import { Global } from '../../services/global';
import { DatePipe } from '@angular/common';

import "jquery";
declare var $: JQueryStatic;

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css'],
  providers: [EventService]
})
export class EventComponent implements OnInit {

  public event: Event;
  public valorations: Valoration;
  public organizer: string;
  public loguedUserId: string;
  public url: string;
  public newValoration: Valoration;
  public isVisitor: boolean;

  public editorConfig: any = {
    toolbarGroups: [
      { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
      { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
      { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
      { name: 'forms', groups: [ 'forms' ] },
      { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
      { name: 'links', groups: [ 'links' ] },
      { name: 'insert', groups: [ 'insert' ] },
      { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
      { name: 'styles', groups: [ 'styles' ] },
      { name: 'colors', groups: [ 'colors' ] },
      { name: 'tools', groups: [ 'tools' ] },
      { name: 'others', groups: [ 'others' ] },
      { name: 'about', groups: [ 'about' ] }
    ],
    removeButtons: 'Source,Save,NewPage,Templates,ExportPdf,Preview,Print,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,NumberedList,BulletedList,Indent,Outdent,Blockquote,CreateDiv,JustifyCenter,JustifyLeft,BidiLtr,BidiRtl,Language,JustifyRight,JustifyBlock,Image,Flash,HorizontalRule,PageBreak,Cut,Copy,Redo,Undo,Paste,PasteText,PasteFromWord,Find,Replace,SelectAll,Link,Unlink,Anchor,Table,Iframe,Styles,Format,Font,FontSize,CopyFormatting,RemoveFormat,BGColor,TextColor,Maximize,About,ShowBlocks'
  }

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _eventService: EventService,
    public _userService: UsersService,
    public _authService: AuthService
  ) {
    this.url = Global.url
    this.newValoration = new Valoration('', '', '', '', null, '');
  }

  ngOnInit(): void {    

    if (localStorage.token) {
      this._userService.getUserId(localStorage.token).subscribe(
        res => {
          this.loguedUserId = res['userId'];

          this._userService.getUser(this.loguedUserId).subscribe(
            res => {
              if (res.user.rol == 'visitor') {
                this.isVisitor = true;
              }
            },
            err => {
              console.log(err);
            }
          )

        },
        err => {
          console.log(err);
        }
      );
    }

    this._route.params.subscribe(params => {
      let id = params['id'];

      this._eventService.getEvent(id).subscribe(
        res => {
          if (res.event) {
            this.event = res.event;
            this.organizer = res.organizerName;
          }
          else {
            this._router.navigate(['/home']);
          }
        },
        err => {
          console.error(err);
          this._router.navigate(['/home']);
        }
      );

      this._eventService.getEventValorations(id).subscribe(
        res => {
          if (res.valorations) {
            this.valorations = res.valorations;
            //console.log(this.valorations)
          }
        },
        err => {
          console.log(err);
        }
      )
    });
  }

  delete(id) {

    if (window.confirm("La información del evento se eliminará. ¿Desea eliminar este evento?")) {
      this._eventService.deleteEvent(id).subscribe(
        res => {
          this._router.navigate(['events']);
        },
        err => {
          console.log(err);
          this._router.navigate(['events']);
        }      
      )
    }
  }

  valorate(){
    this.newValoration.eventId = this.event._id;
    this.newValoration.userId = this.loguedUserId;
    this._eventService.valorate(this.newValoration).subscribe(
      res => {
        if (res['status'] == 'success') {
          //this._router.navigate(['/event', this.event._id]);

          this._eventService.getEventValorations(this.event._id).subscribe(
            res => {
              if (res.valorations) {
                this.valorations = res.valorations;
              }
            },
            err => {
              console.log(err);
            }
          )
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  counter(i: number) {
    return new Array(i);
  }

  dateFormat(date) {
    const datepipe: DatePipe = new DatePipe('en-US')
    return datepipe.transform(date, 'dd/MM/YYYY')
  }

}
