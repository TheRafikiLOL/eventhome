import { Component, OnInit, ViewChild } from '@angular/core';
import { EventService } from 'src/app/services/events.services';
import { UsersService } from 'src/app/services/users.service';
import { Event } from '../../models/event';
import { Router } from '@angular/router';
import { Global } from '../../services/global';
import { CKEditorComponent } from 'ng2-ckeditor';

@Component({
  selector: 'app-event-new',
  templateUrl: '../event/event-form.component.html',
  styleUrls: ['./event-new.component.css'],
  providers: [EventService]
})
export class EventNewComponent implements OnInit {

  public newEvent: Event;
  public status: string;
  public is_edit: boolean;
  public fomr_title: string;

  editorConfig: any = {
    toolbarGroups: [
      { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
      { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
      { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
      { name: 'forms', groups: [ 'forms' ] },
      { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
      { name: 'links', groups: [ 'links' ] },
      { name: 'insert', groups: [ 'insert' ] },
      '/',
      { name: 'styles', groups: [ 'styles' ] },
      { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
      { name: 'colors', groups: [ 'colors' ] },
      { name: 'tools', groups: [ 'tools' ] },
      { name: 'others', groups: [ 'others' ] },
      { name: 'about', groups: [ 'about' ] }
    ],
    removeButtons: 'Source,Save,NewPage,Templates,ExportPdf,Preview,Print,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,NumberedList,BulletedList,Indent,Outdent,Blockquote,CreateDiv,JustifyCenter,JustifyLeft,BidiLtr,BidiRtl,Language,JustifyRight,JustifyBlock,Image,Flash,HorizontalRule,PageBreak'
  }

  afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.jpeg,.png",
    maxSize: "1",
    uploadAPI:  {
      url: Global.url + 'upload-image/'
    },
    theme: "dragNDrop",
    hideProgressBar: false,
    hideResetBtn: true,
    hideSelectBtn: false,
    fileNameIndex: true,
    replaceTexts: {
      selectFileBtn: 'Seleccionar imagen',
      resetBtn: 'Reset',
      uploadBtn: 'Subir imagen',
      dragNDropBox: 'Arrastra imagen aquí',
      afterUploadMsg_success: '¡Imagen subida con éxito!',
      afterUploadMsg_error: '¡Error en la subida!',
      sizeLimit: 'Tamaño máximo'
    }
  };

  constructor(
    private _eventService: EventService,
    private _userService: UsersService,
    private _router: Router
  ) {
    this.newEvent = new Event('','','','',null,null);
    this.is_edit = false;
    this.fomr_title = "Crear nuevo evento";
  }

  ngOnInit(): void {
    // Comprobamos si hay un usuario logueado
    this._userService.getUserId(localStorage.token).subscribe(
      res => {
        // Si el usuario no es organizador se le enviará a la home
        //console.log(res['userId'])

        this._userService.getUser(res['userId']).subscribe(
          response => {
            if (response.user) {
              if (response.user.rol != "organizer") {
                this._router.navigate(['/home']);
              }
            }
            else {
              this._router.navigate(['/home']);
            }
          },
          error => {
            console.error(error);
            this._router.navigate(['/home']);
          }
        )

       },
      err => {
        console.log(err);
        this._router.navigate(['/home']);
      }
    );
  }

  publishEvent(){

    this._userService.getUserId("Bearer " + localStorage.token)
      .subscribe(
        res => {

          this.newEvent.organizerId = res['userId'];

          this._eventService.publishEvent(this.newEvent).subscribe(
            res => {
              if (res['status'] == 'success') {
                this.status == 'success';
                this.newEvent = res['event'];
                this._router.navigate(['/events']);
              }
            },
            err => {
              console.error(err);
              this.status = "error";
            }
          );

        },
        err => {
          console.log(err);
        }
      );

    
  }

  imageUpload(data){
    this.newEvent.image = data.body.image.image;
  }

}
