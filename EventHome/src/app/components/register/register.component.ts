import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

import "jquery";
declare var $: JQueryStatic;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user = {
    name: '',
    email: '',
    password: '',
    passwordSecure: '',
    rol: ''
  }

  constructor(
    private _authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    // Redirigir a la home si se tiene una sesión iniciada
    if(localStorage.token) {
      this.router.navigate(['/']);
    }

    // Cambio de formulario visitor - organizer
    $('#visitor-info').addClass('active-form');
    $('#form-visitor').addClass('active-form');

    $('#change-form').click(function() {
      $('#visitor-info').toggleClass("active-form");
      $('#organizer-info').toggleClass("active-form");
      $('#form-visitor').toggleClass("active-form");
      $('#form-organizer').toggleClass("active-form");

      if ($('#change-form').html() === "Registrarse como organización") {
        $('#change-form').html('Registrarse como visitante');
      } else {
        $('#change-form').html('Registrarse como organización');
      }
    });

    // Cambiar vista de la contraseña
    $('.passVisibility').click(function() {
      if ($(this).hasClass('fa-eye')) {
        $('.password').attr('type', 'text');
        $('.passVisibility').removeClass('fa-eye').addClass('fa-eye-slash');
      }
      else {
        $('.password').attr('type', 'password');
        $('.passVisibility').removeClass('fa-eye-slash').addClass('fa-eye');
      }
    });

    $('.securePassVisibility').click(function() {
      if ($(this).hasClass('fa-eye')) {
        $('.securePassword').attr('type', 'text');
        $('.securePassVisibility').removeClass('fa-eye').addClass('fa-eye-slash');
      }
      else {
        $('.securePassword').attr('type', 'password');
        $('.securePassVisibility').removeClass('fa-eye-slash').addClass('fa-eye');
      }
    });

  }

  registerVisitor() {
    this.user.rol = "visitor";
    this._authService.register(this.user)
      .subscribe(
        res => {
          localStorage.setItem('token', res.token);
          location.reload();
          //this.router.navigate(['/events']);
        },
        err => {
          document.getElementById('errorMesage').innerHTML = (err.error.content);
        }
      );
  }

  registerOrganization() {
    this.user.rol = "organizer";
    this._authService.register(this.user)
      .subscribe(
        res => {
          localStorage.setItem('token', res.token);
          location.reload();
          //this.router.navigate(['/events']);
        },
        err => {
          document.getElementById('errorMesage').innerHTML = (err.error.content);
        }
      );
  }

}