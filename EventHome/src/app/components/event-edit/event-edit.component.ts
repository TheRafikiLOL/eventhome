import { Component, OnInit, ViewChild } from '@angular/core';
import { EventService } from 'src/app/services/events.services';
import { UsersService } from 'src/app/services/users.service';
import { Event } from '../../models/event';
import { ActivatedRoute, Router } from '@angular/router';
import { Global } from '../../services/global';
import { CompileShallowModuleMetadata } from '@angular/compiler';

@Component({
  selector: 'app-event-edit',
  templateUrl: '../event/event-form.component.html',
  styleUrls: ['./event-edit.component.css'],
  providers: [EventService]
})
export class EventEditComponent implements OnInit {

  public newEvent: Event;
  public status: string;
  public is_edit: boolean;
  public fomr_title: string;
  public url: string;
  public loguedUserId: string;

  editorConfig: any = {
    toolbarGroups: [
      { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
      { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
      { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
      { name: 'forms', groups: [ 'forms' ] },
      { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
      { name: 'links', groups: [ 'links' ] },
      { name: 'insert', groups: [ 'insert' ] },
      '/',
      { name: 'styles', groups: [ 'styles' ] },
      { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
      { name: 'colors', groups: [ 'colors' ] },
      { name: 'tools', groups: [ 'tools' ] },
      { name: 'others', groups: [ 'others' ] },
      { name: 'about', groups: [ 'about' ] }
    ],
    removeButtons: 'Source,Save,NewPage,Templates,ExportPdf,Preview,Print,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,NumberedList,BulletedList,Indent,Outdent,Blockquote,CreateDiv,JustifyCenter,JustifyLeft,BidiLtr,BidiRtl,Language,JustifyRight,JustifyBlock,Image,Flash,HorizontalRule,PageBreak'
  }

  afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.jpeg,.png",
    maxSize: "1",
    uploadAPI:  {
      url: Global.url + 'upload-image/'
    },
    theme: "dragNDrop",
    hideProgressBar: false,
    hideResetBtn: true,
    hideSelectBtn: false,
    fileNameIndex: true,
    replaceTexts: {
      selectFileBtn: 'Seleccionar imagen',
      resetBtn: 'Reset',
      uploadBtn: 'Subir imagen',
      dragNDropBox: 'Arrastra imagen aquí',
      afterUploadMsg_success: '¡Imagen subida con éxito!',
      afterUploadMsg_error: '¡Error en la subida!',
      sizeLimit: 'Tamaño máximo'
    }
  };

  constructor(
    private _eventService: EventService,
    private _userService: UsersService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.newEvent = new Event('','','','',null,null);
    this.is_edit = true;
    this.fomr_title = "Editar evento";
    this.url = Global.url;
  }

  ngOnInit(): void {
    this._userService.getUserId(localStorage.token).subscribe(
      res => {
        this.loguedUserId = res['userId'];
        this.getEvent();
      },
      err => {
        console.log(err);
        this._router.navigate(['/home']);
      }
    );
  }

  publishEvent(){

    this._userService.getUserId("Bearer " + localStorage.token)
      .subscribe(
        res => {
          this.newEvent.organizerId = res['userId'];

          this._eventService.updateEvent(this.newEvent._id, this.newEvent).subscribe(
            res => {
              if (res['status'] == 'success') {
                this.status == 'success';
                this.newEvent = res['eventUpdated'];
                this._router.navigate(['/event/', this.newEvent._id]);
              }
            },
            err => {
              console.error(err);
              this.status = "error";
            }
          );

        },
        err => {
          console.log(err);
        }
      );
  }

  imageUpload(data){
    this.newEvent.image = data.body.image.image;
  }

  getEvent(){
    this._route['params'].subscribe(params => {
      let id = params['id'];

      this._eventService.getEvent(id).subscribe(
        response => {
          if (response.event) {
            if (response.event.organizerId != this.loguedUserId) {
              this._router.navigate(['/home']);
            }
            this.newEvent = response.event;
          }
          else {
            this._router.navigate(['/home']);
          }
        },
        error => {
          console.error(error);
          this._router.navigate(['/home']);
        }
      )
    });
  }

}
