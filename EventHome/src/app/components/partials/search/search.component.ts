import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import "jquery";
declare var $: JQueryStatic;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public searchString: string;

  constructor(
    private _router: Router,
  ) { }

  ngOnInit(): void {
    // Search position
    $('#slider').append($('#search-container'));
  }

  searchEvents(){
    this._router.navigate(['/events/search', this.searchString]);
  }

}
