import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public userData: any;

  constructor(
    public authService: AuthService,
    private userService: UsersService) { }

  ngOnInit(): void {
    
    // Responsive nav
    const burgerButton = document.querySelector('.burger-button');
    const nav = document.querySelector('.nav-links');
    
    burgerButton.addEventListener('click', () => {
      nav.classList.toggle('active');
      burgerButton.classList.toggle('active');
    });

    // Get sesions user ID
    if (localStorage.token) {
      this.userService.getUserId("Bearer " + localStorage.token)
      .subscribe(
        res => {
          this.userData = res;
        },
        err => {
          console.log(err);
        }
      );
    }
  }

}
