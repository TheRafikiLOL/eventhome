import { Component, OnInit } from '@angular/core';
import { Event } from '../../../models/event';
import { EventService } from '../../../services/events.services';
import { Global } from '../../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {IvyCarouselModule} from 'angular-responsive-carousel';

@Component({
  selector: 'app-events-last',
  templateUrl: './events-last.component.html',
  styleUrls: ['./events-last.component.css'],
  providers: [EventService],
})
export class EventsLastComponent implements OnInit {

  public events: Array<Event>;
  public url: string;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _eventService: EventService
  ) {
    this.url = Global.url
  }

  ngOnInit(): void {

    this._eventService.getLastEvents().subscribe(
      res => {
        if (res['events']) {
          this.events = res['events'];
        }
        else {

        }
      },
      err => {
        console.error(err);
      }
    )

  }

}
