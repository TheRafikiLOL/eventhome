import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { Event } from '../../models/event';
import { UsersService } from 'src/app/services/users.service';
import { EventService } from 'src/app/services/events.services';
import { AuthService } from 'src/app/services/auth.service';
import { Global } from '../../services/global';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [UsersService, EventService]
})
export class ProfileComponent implements OnInit {

  public profile: User;
  public organizerEvents: Event;
  public loguedUserId: string;
  public url: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    public _userService: UsersService,
    public _eventService: EventService,
    public _authService: AuthService
  ) {
    this.url = Global.url
  }

  ngOnInit(): void {

    let token = localStorage.token;

    this._userService.getUserId(token).subscribe(
      res => {
        this.loguedUserId = res['userId'];
      },
      err => {
        console.log(err);
      }
    );

    this._route.params.subscribe(params => {
      let id = params['id'];

      this._userService.getUser(id).subscribe(
        res => {
          if (res.user) {
            this.profile = res.user;

            if (this.profile.rol == 'organizer') {
              this._eventService.getOrganizerEvents(this.profile._id).subscribe(
                res => {
                  if (res['events']) {
                    this.organizerEvents = res['events'];
                  }
                },
                err => {
                  console.error(err);
                }
              )
            }
          }
          else {
            this._router.navigate(['/home']);
          }
        },
        err => {
          console.error(err);
          this._router.navigate(['/home']);
        }
      )
    });
  }

  delete() {
    if (localStorage.token) {
      this._userService.getUserId(localStorage.token).subscribe(
        res => {
          this._userService.deleteUser(res['userId']).subscribe(
            res => {
              this._authService.logout();
            },
            err => {
              console.log(err);
              this._router.navigate(['home']);
            })

        },
        err => {
          console.log(err);
        }
      );
    }

  }

  dateFormat(date) {
    const datepipe: DatePipe = new DatePipe('en-US')
    return datepipe.transform(date, 'dd/MM/YYYY')
  }

}