import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';

import "jquery";
declare var $: JQueryStatic;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = {
    email: '',
    password: ''
  }

  constructor(
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    // Redirigir a la home si se tiene una sesión iniciada
    if(localStorage.token) {
      this.router.navigate(['/']);
    }

    // Cambiar vista de la contraseña
    $('#passVisibility').click(function() {
      if ($(this).hasClass('fa-eye')) {
        $('.password').attr('type', 'text'); 
        $(this).removeClass('fa-eye').addClass('fa-eye-slash');
      }
      else {
        $('.password').attr('type', 'password'); 
        $(this).removeClass('fa-eye-slash').addClass('fa-eye');
      }
    });

  }

  login() {
    this.authService.login(this.user)
      .subscribe(
        res => {
          localStorage.setItem('token', res.token);
          location.reload();
          //this.router.navigate(['/events']);
        },
        err => {
          document.getElementById('errorMesage').innerHTML = (err.error.content);
        }        
      );
  }

}
