import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Event } from '../../models/event';
import { Global } from '../../services/global';
import { EventService } from '../../services/events.services';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-events-search',
  templateUrl: './events-search.component.html',
  styleUrls: ['./events-search.component.css'],
  providers: [EventService]
})
export class EventsSearchComponent implements OnInit {

  public events: Event[];
  public search: String;
  public url: string;

  constructor(
    private _route: ActivatedRoute,
    private _eventService: EventService
  ) {
    this.url = Global.url
  }

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      this.search = params['search'];

      this._eventService.search(this.search).subscribe(
        res => {
          if(res.events) {
            this.events = res.events;
            console.log(this.events)
          }
        },
        err => {
          console.log(err);
        }
      )
      
    })
  }

  dateFormat(date) {
    const datepipe: DatePipe = new DatePipe('en-US')
    return datepipe.transform(date, 'dd/MM/YYYY')
  }

}
