import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.css'],
  providers: [UsersService]
})
export class OrganizationsComponent implements OnInit {

  public organizers: Array<User>;

  constructor(
    private _userService: UsersService
  ) { }

  ngOnInit(): void {
    this._userService.getOrganizers().subscribe(
      response => {
        if (response.organizers) {
          this.organizers = response.organizers;
        }
        else {
          
        }
      },
      error => {
        console.error(error);
      }
    )
  }

}
